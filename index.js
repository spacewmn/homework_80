const express = require("express");
const mysql = require("mysql");
const config = require("./config");
const items = require("./app/items");
const categories = require("./app/categories");
const location = require("./app/location");
const db = require('./mysql')
const app = express();
const port = 8000;

const connection = mysql.createConnection(config.db);

app.use(express.static('public'));
app.use(express.json());

connection.connect((err) => {
    if (err) {
        console.log(err);
        throw err;
    }

    app.use('/items', items(db(connection)));
    app.use('/categories', categories(db(connection)));
    app.use('/location', location(db(connection)));

    console.log('connected to mysql');
    app.listen(port, () => {
        console.log('Server started at http://localhost:' + port);
    });
});


