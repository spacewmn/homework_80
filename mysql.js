module.exports = (db) => ({
    getItems(resource) {
        return new Promise((res, rej) => {
            db.query('SELECT * FROM ??', [resource], (err, result) => {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    },
    getItem(resource, id) {
        return new Promise((res, rej) => {
            db.query('SELECT * FROM ?? WHERE id = ?', [resource, id], (err, result) => {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    },
    createItem(resource, data) {
        return new Promise((res, rej) => {
            db.query("INSERT INTO ?? SET ?", [resource, data], (err, result) => {
                if (err) {
                    rej(err);
                }
                data.id = result.insertId;
                res(data);
            });
        })
    },
    deleteItem(resource, id) {
        return new Promise((res, rej) => {
            db.query('DELETE FROM ?? WHERE id = ?', [resource, id], (err, result) => {
                if (err) {
                    rej(err);
                }
                res(result);
            });
        });
    },

    updateItem(resource, data, id) {
        console.log(data);
        return new Promise((res, rej) => {
            db.query('UPDATE ?? SET ? WHERE id = ?', [resource, data, id], (err, result) => {
                if (err) {
                    console.log(err);
                    rej(err)
                };
                data.id = result.insertId;
                res(data);
            })
        })
    }
});