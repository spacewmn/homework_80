CREATE DATABASE IF NOT EXISTS inventarisation;
Use inventarisation;

CREATE TABLE IF NOT EXISTS categories (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_category VARCHAR(45) NOT NULL UNIQUE,
  description_category VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS location (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_location VARCHAR(45) NOT NULL UNIQUE,
  description_location VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS items (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  item_name VARCHAR(255) NOT NULL,
  category_id INT NOT NULL,
  CONSTRAINT FK_category
  FOREIGN KEY (category_id)
  REFERENCES categories (id)
  ON DELETE RESTRICT        
  ON UPDATE CASCADE,
  location_id INT NOT NULL,
  CONSTRAINT FK_location
  FOREIGN KEY (location_id)
  REFERENCES location (id)
  ON DELETE RESTRICT        
  ON UPDATE CASCADE,
  item_description TEXT(3000),
  image VARCHAR(40),
  date_supply DATETIME NOT NULL 
  );

  INSERT IGNORE INTO categories (name_category, description_category)
  VALUES ("Furniture", "Soft Furnitures, tables, chairs"), 
  ("Computer equipment", "PC, laptops, printers");

  INSERT INTO location (name_location, description_location)
  VALUES ("Canteen", "Big hall on 1 floor"), 
  ("Director's office", "Room № 1 on 5 floor"), 
  ("Office", "Working place 2-4 floors");
   
  INSERT INTO items (item_name, category_id, location_id, item_description, date_supply)
  VALUES ("Laptop HP", 2, 3, "Display. Size. 15.60-inch. Resolution. Processor. Processor. Intel Core i5 8250U. Base Clock Speed.", "2020-01-01 10:10:10"),
  ("Dinner table", 1, 2, "Wood oak table. Parameters 100x200x100", "2020-01-01 10:10:10");