const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', async (req, res) => {
        try {
            const location = await db.getItems('location');
            const arr = location.map(place => {
                return {'id': place.id, 'name': place.name_location}
            });
            res.send(arr);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        const response = await db.getItem('location', req.params.id);
        res.send(response[0]);
    });

    router.post('/', async (req, res) => {
        let place = req.body;
        if (!place.name_location) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        const newLocation = await db.createItem('location', place);
        res.send(newLocation);
    });

    router.delete('/:id', async (req, res) => {
        try {
            const response = await db.deleteItem('location', req.params.id);
            res.send(response[0]);
        } catch (e) {
            res.send('Cannot be deleted')
        }
    });

    router.put('/:id', async (req, res) => {
        let place = req.body;
        if (!place.name_location) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        const newLocation = await db.updateItem('location', place, req.params.id);
        res.send(newLocation);
    });

    return router;
};

module.exports = createRouter;