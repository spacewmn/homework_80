const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', async (req, res) => {
        try {
            const categories = await db.getItems('categories');
            const arr = categories.map(category => {
                return {'id': category.id, 'name': category.name_category}
            });
            res.send(arr);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        const response = await db.getItem('categories', req.params.id);
        res.send(response[0]);
    });

    router.post('/', async (req, res) => {
        let category = req.body;
        if (!category.name_category) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        const newCategory = await db.createItem('categories', category);
        res.send(newCategory);
    });

    router.delete('/:id', async (req, res) => {
        try {
            const response = await db.deleteItem('categories', req.params.id);
            res.send(response[0]);
        } catch (e) {
            res.send('Cannot be deleted')
        }
    });

    router.put('/:id', async (req, res) => {
        let category = req.body;
        if (!category.name_category) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        const newCategory = await db.updateItem('categories', category, req.params.id);
        res.send(newCategory);
    });
    return router;
};

module.exports = createRouter;