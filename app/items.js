const express = require('express');
const {nanoid} = require('nanoid');
const multer  = require('multer');
const config = require('../config');
const router = express.Router();
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    router.get('/', async (req, res) => {
        try {
            const items = await db.getItems('items');
            const arr = items.map(item => {
                return {'id': item.id, 'name': item.item_name}
            });
            res.send(arr);
        } catch (e) {
            res.status(500).send(e);
        }
    });

    router.get("/:id", async (req, res) => {
        const response = await db.getItem('items', req.params.id);
        res.send(response[0]);
    });

    router.post('/', upload.single('image'), async (req, res) => {
        let item = req.body;
        if (!item.item_name || !item.category_id || !item.location_id || !item.date_supply) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        if (req.file) {
            item.image = req.file.filename;
        }
        const newItem = await db.createItem('items', item);
        res.send(newItem);
    });

    router.delete('/:id', async (req, res) => {
        const response = await db.deleteItem('items', req.params.id);
        res.send(response[0]);
    });

    router.put('/:id', upload.single('image'), async (req, res) => {
        let item = req.body;
        if (!item.item_name || !item.category_id || !item.location_id || !item.date_supply) {
            return res.status(404).send({error: "Full all required fields!"})
        }
        if (req.file) {
            item.image = req.file.filename;
        }
        const newItem = await db.updateItem('items', item, req.params.id);
        res.send(newItem);
    });
    return router;
};

module.exports = createRouter;